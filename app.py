from flask import Flask, abort
from flask_restful import Api, Resource, inputs, reqparse
from flask_sqlalchemy import SQLAlchemy
import sys
import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///instance\My_database.db'

db = SQLAlchemy(app)


class EventByID(Resource):
    def get(self, event_id):
        event = Car.query.filter(Car.id == event_id).first()
        if event is None:
            abort(404, "The event doesn't exist!")
        return {"id": event.id,
                "event": event.event,
                'date': str(event.date)}

    def delete(self, event_id):
        event = Car.query.filter(Car.id == event_id).first()
        if event is None:
            abort(404, "The event doesn't exist!")
        db.session.delete(event)
        db.session.commit()
        return {'message': "The event has been deleted!"}


class HelloWorldResource2(Resource):
    def get(self):
        args = Car.query.filter(Car.date == datetime.date.today()).all()
        if len(args) == 0:
            return {"message": "There are no events for today!"}
        else:
            return maker(args)


class HelloWorldResource(Resource):
    def get(self):
        parser_ = reqparse.RequestParser()
        parser_.add_argument("start_time", type=inputs.date)
        parser_.add_argument("end_time", type=inputs.date)
        params = parser_.parse_args()
        start_time, end_time = params.start_time, params.end_time
        if start_time is None and end_time is None:
            args = Car.query.all()
            if len(args) == 0:
                return {"message": "There are no events for today!"}
            else:
                return maker(args)
        else:

            args = Car.query.filter(Car.date >= start_time.date(), Car.date <= end_time.date())
            return maker(args)

    def post(self):
        args = parser.parse_args()
        car = Car(event=args['event'], date=args['date'].date())
        db.session.add(car)
        db.session.commit()
        return {"message": "The event has been added!",
                "event": args['event'],
                'date': str(args['date'].date())}


class Car(db.Model):
    __tablename__ = 'calendar'
    id = db.Column(db.Integer, primary_key=True)
    event = db.Column(db.String(80), nullable=False)
    date = db.Column(db.Date, nullable=False)


def maker(args):
    list_ = list()
    for k in args:
        list_.append({'id': k.id,
                      'event': k.event,
                      'date': str(k.date)})
    return list_


api = Api(app)
api.add_resource(HelloWorldResource, '/event', '/event?start_time=<string:start_time_>&end_time=<string:end_time_>')
api.add_resource(HelloWorldResource2, '/event/today')
api.add_resource(EventByID, '/event/<int:event_id>')


db.create_all()

parser = reqparse.RequestParser()
parser.add_argument(
    'date',
    type=inputs.date,
    help="The event date with the correct format is required! The correct format is YYYY-MM-DD!",
    required=True
)
parser.add_argument(
    'event',
    type=str,
    help="The event name is required!",
    required=True
)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        arg_host, arg_port = sys.argv[1].split(':')
        app.run(host=arg_host, port=arg_port)
    else:
        app.run()
