Данный проект направлен на знакомство с построением своего Restful API и работы с ним.

Для запуска необходимо скачать и запустить файл app.py.

После чего переходим по ссылке http://127.0.0.1:5000/event
Будут отображены все события, хранящиеся в базе данных.
По ссылке http://127.0.0.1:5000/event/today можно увидеть задания на сегодня.
Если хотим обратиться к конкретному заданию, то http://127.0.0.1:5000/event/<id>

Интерфейса у приложения на данный момент нет, поэтому добавление в базу данных происходит вручную, с помощью сторонних приложений.
(использовал DB Browser for SQLite).

P.s. В ближайшем будущем планирую доделать интерфейс для удобного добавления в базу данных и удаления из неё.